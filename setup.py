import os
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))


setup(name='PyOCL',
      version='0.1',
      description='simple utility wrapper around pyopencl',
      url='http://mweigert@bitbucket.org/mweigert/pyocl',
      author='Martin Weigert',
      author_email='mweigert@mpi-cbg.de',
      license='MIT',
      packages=['PyOCL'],
      package_data={'PyOCL': ['kernels/*']},
      install_requires=[
          'numpy', 'pyopencl'],
      zip_safe=False)
