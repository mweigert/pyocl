
from PyOCL import OCLProcessor,OCLDevice,cl, test_opencl
import numpy as np
from time import time

print "---------------------\n"
print "availbale OpenCL platform(s):"

print cl.get_platforms()

print "---------------------\n"


test = test_opencl.TestBasic()

test.setUp()

N = 4

for Nx,Ny in zip(*((2**np.arange(8,13),)*2)):

    try:
        t = time()
        out = test.test_mean(Nx,Ny,N)
        print "test %sx%s mean filter:\t image size [%s x %s] \t time to run: %.1f ms"%(2*N+1,2*N+1,Nx,Ny,1000.*(time()-t))

    except Exception as e:
        print e
        print "copy Img to Img failed!"

# N = 10

# for Nx,Ny,Nz in zip(*((2**np.arange(4,9),)*3)):

#     try:
#         t = time()
#         out = test.test_mean3(Nx,Ny,Nz,N)
#         print "test %sx%sx%s mean filter:\t image size [%s x %s x%s] \t time to run: %.1f ms"%(N,N,N,Nx,Ny,Nz,1000.*(time()-t))

#     except Exception as e:
#         print e
#         print "copy Img to  Buffer failed!"
