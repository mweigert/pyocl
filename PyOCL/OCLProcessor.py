'''
@author: mweigert

A basic wrapper class around pyopencl to handle image manipulation via OpenCL
basic usage:


    #create a device
    dev = OCLDevice(useGPU=True, useDevice = 0, printInfo = True)

    #create a OpenCL buffers/imgs
    img = dev.createImage((N,N), dtype = float32 )
    buf = dev.createBuffer(N*N, dtype = float32)

    # setup processor for device from opencl kernel file
    proc = OCLProcessor(dev,"kernel.cl")

    # write to device
    dev.writeImage(img,data)

    # compute
    proc.runKernel("kernelName",(N,N),None,img,buf,**params)

    # read from device
    out = dev.readBuffer(buf, dtype = float32).reshape(img.shape)



'''


import logging
logger = logging.getLogger(__name__)



import numpy as np
import pyopencl as cl


cl_datatype_dict = {cl.channel_type.FLOAT:np.float32,
                    cl.channel_type.UNSIGNED_INT8:np.uint8,
                    cl.channel_type.UNSIGNED_INT16:np.uint16,
                    cl.channel_type.SIGNED_INT8:np.int8,
                    cl.channel_type.SIGNED_INT16:np.int16,
                    cl.channel_type.SIGNED_INT32:np.int32}


cl_datatype_dict.update({dtype:cltype for cltype,dtype in cl_datatype_dict.iteritems()})


class OCLDevice:
    """ a wrapper class representing a CPU/GPU device"""

    def __init__(self,initCL = True, **kwargs):
        """ same kwargs as initCL """
        if initCL:
            self.initCL(**kwargs)

    def initCL(self,useDevice = 0, useGPU = True, printInfo = False, context_properties= None):
        platforms = cl.get_platforms()
        if len(platforms) == 0:
            raise Exception("Failed to find any OpenCL platforms.")
            return None

        devices = []
        if useGPU:
            devices = platforms[0].get_devices(cl.device_type.GPU)
            if len(devices) == 0:
                logger.warning("Could not find GPU device...")
        else:
            devices = platforms[0].get_devices(cl.device_type.CPU)
            if len(devices) == 0:
                logger.warning("Could neither find GPU nor CPU device....")

        if len(devices) ==0:
            logger.warning("couldnt find any devices...")
            return None
        else:
            logger.info("using device: %s"%devices[useDevice].name)

        # Create a context using the nth device
        self.context = cl.Context(devices = [devices[useDevice]],properties = context_properties)

        self.device =  devices[useDevice]

        self.queue = cl.CommandQueue(self.context,properties = cl.command_queue_properties.PROFILING_ENABLE)

        if printInfo:
            self.printInfo()


    def printInfo(self):
        platforms = cl.get_platforms()
        for p in platforms:
            print "platform: \t",p.name
            printNames = [["CPU",cl.device_type.CPU],
                          ["GPU",cl.device_type.GPU]]
            for name, identifier in printNames:
                print "device type: \t" , name
                try:
                    for d in p.get_devices(identifier):
                        print "\t ", d.name
                except:
                    print "nothing found: ", name

        infoKeys = ['NAME','GLOBAL_MEM_SIZE','GLOBAL_MEM_SIZE','MAX_MEM_ALLOC_SIZE','LOCAL_MEM_SIZE','IMAGE2D_MAX_WIDTH',
                    'IMAGE2D_MAX_HEIGHT','IMAGE3D_MAX_WIDTH',
                    'IMAGE3D_MAX_HEIGHT','IMAGE3D_MAX_DEPTH','MAX_WORK_GROUP_SIZE','MAX_WORK_ITEM_SIZES']
        for k in infoKeys:
            print "%s: \t  %s"% (k, self.device.get_info(getattr(cl.device_info,k)))


    def createBuffer(self,N,mem_flags = cl.mem_flags.READ_ONLY, dtype = np.uint16 ):
        """creates a cl buffer memory object"""
        size = np.dtype(dtype).itemsize * N
        mf = cl.mem_flags

        return cl.Buffer(self.context, mem_flags , size= size)

    def writeBuffer(self,clBuf,data):
        """writes data to a cl buffer memory object
        make sure that data.dtype is identical to dtype the buffer was created with
        """

        if data.nbytes != clBuf.size:
            raise IndexError("writeBuffer: wrong length!")
        else:
            cl.enqueue_write_buffer(self.queue,clBuf,data.flatten()).wait()

    def readBuffer(self,clBuf,dtype = np.uint16):
        """reads data back from a cl buffer memory object
        """

        size = clBuf.size/np.dtype(dtype).itemsize
        out = np.zeros(size,dtype = dtype)
        cl.enqueue_read_buffer(self.queue,clBuf,out).wait()
        return out


    def createImage(self,dim, mem_flags = cl.mem_flags.READ_ONLY,
                    dtype = np.uint16,
                    channel_type = None,
                    channel_order = cl.channel_order.R):
        """creates a cl image memory object
        channel_type (if given) overwrites dtype
        e.g. channel_type = cl.channel_type.UNSIGNED_INT16,
        """
        if not channel_type:
            channel_type = cl_datatype_dict[dtype]

        fmt = cl.ImageFormat(channel_order, channel_type)

        return cl.Image(self.context,mem_flags,
                      fmt,shape = dim)

    def createImage_like(self,data, dtype= None,mem_flags= "READ_ONLY"):
        """
        mem_flags = "READ_ONLY", "READ_WRITE"
        
        """
        mem_dict = {"READ_ONLY":cl.mem_flags.READ_ONLY,
                    "READ_WRITE":cl.mem_flags.READ_WRITE}

        mem_flags = mem_dict[mem_flags]

        if not dtype:
            dtype = data.dtype.type
        channel_order = cl.channel_order.R
        return self.createImage(data.shape[::-1],dtype = dtype,
                                mem_flags = mem_flags,
                                channel_order= channel_order)


    def writeImage(self,clImg, data):

        if hasattr(clImg,"shape"):
            imgShape = clImg.shape
        else:
            imgShape = (clImg.width,)

        foo = len(imgShape)
        dshape = data.shape
        if clImg.format.channel_order in [cl.channel_order.RGBA,
                                          cl.channel_order.BGRA]:
            dshape = dshape[1:]


        if dshape != imgShape[::-1]:
            print "writeImage: wrong shape!",data.shape[::-1],imgShape
        else:
            cl.enqueue_write_image(self.queue,clImg,[0]*foo,imgShape,data).wait()



    def readImage(self,clImg,dtype = None):
        img_shape = clImg.shape
        arr_shape = clImg.shape[::-1]

        if clImg.format.channel_order in [cl.channel_order.RGBA,
                          cl.channel_order.BGRA]:
            img_shape += (4,)
            arr_shape += (4,)

        if clImg.format.channel_order in [cl.channel_order.RGB,
                          cl.channel_order.RGx]:
            img_shape += (3,)
            arr_shape += (3,)

        if clImg.format.channel_order in [cl.channel_order.RG,
                          cl.channel_order.RA]:
            img_shape += (2,)
            arr_shape += (2,)

        try:
            dtype = cl_datatype_dict[clImg.format.channel_data_type]
        except Exception as e:
            print e
            raise TypeError("dtype not given and not available yet!")


        out = np.zeros(img_shape,dtype=dtype)
        cl.enqueue_read_image(self.queue,clImg,[0]*len(clImg.shape),clImg.shape,out)

        return out.reshape(arr_shape)




class OCLProcessor:
    """ a wrapper class to represent processing functions on 2d/3d data"""
    def __init__(self,clDev=None, kernelFileName=None,options=""):
        self.initCL(clDev)
        if kernelFileName:
            self.compileKernel(open(kernelFileName).read(),options=options)

    def initCL(self,clDev):
        self.clDev = clDev

    def compileKernel(self,kernelStr, options=""):
        self.prg = cl.Program(self.clDev.context,kernelStr).build(options=options)


    # somehow nd_range_kernel doesnt wortk so we have to set arguments
    # in runKernel
    # def setArgs(self,kernelName,*args):
    #     try:
    #         kernel = getattr(self.prg,kernelName)
    #     except:
    #         print "not a valid kernel name: ", kernelName
    #         return

    #     kernel.set_args(*args)


    def runKernel(self,kernelName, globalSize, localSize = None,*args):
        try:
            kernel = getattr(self.prg,kernelName)
        except:
            raise KeyError("not a valid kernel name: %s"%kernelName)

        return kernel(self.clDev.queue,globalSize,localSize,*args)



if __name__ == '__main__':
    clDev = OCLDevice(useGPU = False, printInfo = True)
