__kernel void copyImgToBuffer(__read_only image2d_t input, __global short* output,const int Nx)
{
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i = get_global_id(0);
  uint j = get_global_id(1);

  uint pix = read_imageui(input,sampler,(int2)(i,j)).x;
   


  output[i+Nx*j] = (short)(pix);
}


__kernel void copyImgToImg(__read_only image2d_t input, __write_only image2d_t output)
{
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i = get_global_id(0);
  uint j = get_global_id(1);

  float pix = read_imagef(input,sampler,(int2)(i,j)).x;

  write_imagef(output,(int2)(i,j),(float4)(pix,0,0,0));
}


__kernel void meanFilter(__read_only image2d_t input, __write_only image2d_t output, const int N)
{
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i = get_global_id(0);
  uint j = get_global_id(1);

  float sum = 0.f;
  for (int i2 = -N; i2 <= N; ++i2)
	for (int j2 = -N; j2 <= N; ++j2)
      sum += read_imagef(input,sampler,(int2)(i+i2,j+j2)).x;

  write_imagef(output,(int2)(i,j),(float4)(sum,0,0,0));
}


// __kernel void copyImgToImgFloat3(__read_only image3d_t input, __write_only image3d_t output)
// {
//   const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

//   uint i = get_global_id(0);
//   uint j = get_global_id(1);
//   uint k = get_global_id(2);

//   float pix = read_imagef(input,sampler,(int4)(i,j,k,0)).x;

  
//   write_imagef(output,(int4)(i,j,k,0),(float4)(pix,0,0,0));
// }

__kernel void copyImgToImgShort2(__read_only image2d_t input, __write_only image2d_t output)
{
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i = get_global_id(0);
  uint j = get_global_id(1);

  int pix = read_imagef(input,sampler,(int2)(i,j)).x;

  write_imagef(output,(int2)(i,j),(uint)(3));
  
}

