/*
A simple bilateral filter which averages pixel with their neighborhood proportional to distance  
*/



__kernel void run2d(__read_only image2d_t input, __global short* output,const int Nx, const int Ny,const int FSIZE, const float SIGMA)
{
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i = get_global_id(0);
  uint j = get_global_id(1);

  float res = 0;
  float sum = 0;

  for(int k = -FSIZE;k<=FSIZE;k++){
    for(int m = -FSIZE;m<=FSIZE;m++){

    uint4 pix1 = read_imageui(input,sampler,(int2)(i+k,j+m));
    float weight = exp(-1.f/SIGMA/SIGMA*(k*k+m*m));
    res += pix1.x*weight;
    sum += weight;

    }
  }

  output[i+Nx*j] = (short)(res/sum);
}



__kernel void run3d(__read_only image3d_t input, __global short* output,const int Nx, const int Ny,const int Nz,const int FSIZE, const float SIGMA)
{

    const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i = get_global_id(0);
  uint j = get_global_id(1);
  uint k = get_global_id(2);
  

  uint pix0 = read_imageui(input,sampler,(int4)(i,j,k,0)).x;
  
  float res = 0;
  float sum = 0;


  for(int i2 = -FSIZE;i2<=FSIZE;i2++){
	for(int j2 = -FSIZE;j2<=FSIZE;j2++){
	  for(int k2 = -FSIZE;k2<=FSIZE;k2++){
	
		uint pix1 = read_imageui(input,sampler,(int4)(i+i2,j+j2,k+k2,0)).x;
		float weight = exp(-1.f/SIGMA/SIGMA*(i2*i2+j2*j2+k2*k2));
		res += pix1*weight;
		sum += weight;
	  }
	}
  }
  
  output[i+j*Nx+k*Nx*Ny] = (short)(res/sum);

}
