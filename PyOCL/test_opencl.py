'''
@author: mweigert

tests opencl functionality and some filters (bilateral and non local means)
'''


import os
import numpy as np
import numpy.testing as npt

from OCLProcessor import OCLDevice, OCLProcessor
import pyopencl as cl

def absPath(s):
    return os.path.join(os.path.dirname(__file__),s)


class TestBasic:
    def setUp(self):
        self.dev = OCLDevice(printInfo = True, useDevice=0)

    def copyImg(self,data):
        proc = OCLProcessor(self.dev,absPath("kernels/test_kernels.cl"))

        imgIn = self.dev.createImage(data.shape[::-1],
                dtype = np.float32)

        imgOut = self.dev.createImage(data.shape[::-1],
                dtype = np.float32,
                mem_flags=cl.mem_flags.READ_WRITE)

        self.dev.writeImage(imgIn,data)

        proc.runKernel("copyImgToImg",imgIn.shape,None,imgIn,imgOut)
        return self.dev.readImage(imgOut,dtype = np.float32)

    def test_mean(self,Nx=200,Ny=200,N=2):
        data = np.arange(Nx*Ny,dtype = np.float32).reshape((Ny,Nx))

        proc = OCLProcessor(self.dev,absPath("kernels/test_kernels.cl"))

        imgIn = self.dev.createImage_like(data)

        imgOut = self.dev.createImage_like(data, mem_flags = "READ_WRITE")

        self.dev.writeImage(imgIn,data)

        proc.runKernel("meanFilter",imgIn.shape,None,imgIn,imgOut,np.int32(N))
        return self.dev.readImage(imgOut,dtype = np.float32)

    # def test_mean3(self,Nx=64,Ny=64,Nz= 64, N=2):
    #     data = np.ones((Nz,Ny,Nz),dtype=np.float32)
    #     h = np.ones((N,)*3)
    #     return convolve._convolve3_img(self.dev,data,h)


    def test_img(self,Nx=100,Ny=50):
        data = np.arange(Nx*Ny,dtype = np.float32).reshape((Ny,Nx))
        out = self.copyImg(data)
        npt.assert_almost_equal(out, data)
        return out


    # def test_img3d(self):
    #     Nx, Ny, Nz = 100,50, 40
    #     data = np.arange(Nx*Ny*Nz,dtype = np.float32).reshape((Nz,Ny,Nx))

    #     proc = OCLProcessor(self.dev,"test_kernels.cl")

    #     imgIn = self.dev.createImage(data.shape[::-1],
    #                                  dtype = np.float32)

    #     imgOut = self.dev.createImage(data.shape[::-1],\
    #                 mem_flags=cl.mem_flags.READ_WRITE,
    #                 dtype = np.float32)
    #     self.dev.writeImage(imgIn,data)

    #     proc.runKernel("copyImgToImgFloat3",imgIn.shape,None,imgIn,imgOut)
    #     out = self.dev.readImage(imgOut,dtype = np.float32)
    #     npt.assert_almost_equal(out, data)
    #     return out

    def test_img2dShort(self):
        Nx, Ny = 100,50
        data = np.arange(Nx*Ny,dtype = np.uint16).reshape((Ny,Nx))

        proc = OCLProcessor(self.dev,absPath("kernels/test_kernels.cl"))

        imgIn = self.dev.createImage(data.shape[::-1],
                                     dtype = np.uint16,
                    mem_flags=cl.mem_flags.READ_WRITE)

        imgOut = self.dev.createImage(data.shape[::-1],\
                    mem_flags=cl.mem_flags.READ_WRITE)

        self.dev.writeImage(imgIn,data)

        proc.runKernel("copyImgToImgShort2",imgIn.shape,None,imgIn,imgOut)
        out = self.dev.readImage(imgOut,dtype = np.uint16)
        # npt.assert_almost_equal(out, data)
        return out


    def test_img_float(self):

        data = np.ones([128,128]).astype(np.float32)

        img = self.dev.createImage(data.shape[::-1],channel_type = cl.channel_type.FLOAT,mem_flags = cl.mem_flags.READ_WRITE)
        self.dev.writeImage(img,data.astype(np.float32))
        out = self.dev.readImage(img,dtype = np.float32)
        npt.assert_almost_equal(out, data)



class TestFilters:
    def setUp(self):
       self.dev = OCLDevice(printInfo = True)


    def test_bilateral2d(self):
        proc = OCLProcessor(self.dev,absPath("kernels/bilateral.cl"))

        data = np.ones((256,)*2).astype(np.uint16)

        clImg = self.dev.createImage(data.shape[::-1])
        clBuf = self.dev.createBuffer(data.size)
        self.dev.writeImage(clImg,data.astype(np.uint16))

        FSIZE, SIGMA = 3, 50

        proc.runKernel("run2d",clImg.shape,None,clImg,clBuf,
                         np.int32(clImg.shape[0]),np.int32(clImg.shape[1]),np.int32(FSIZE),np.float32(SIGMA))

        return self.dev.readBuffer(clBuf).reshape(clImg.shape).transpose()


    def test_bilateral3d(self):
        proc = OCLProcessor(self.dev,absPath("kernels/bilateral.cl"))


        data = np.ones((128,)*3).astype(np.uint16)

        clImg = self.dev.createImage(data.shape[::-1])
        clBuf = self.dev.createBuffer(data.size)
        self.dev.writeImage(clImg,data.astype(np.uint16))

        FSIZE, SIGMA = 4, 2000

        proc.runKernel("run3d",clImg.shape,None,clImg,clBuf,
                         np.int32(clImg.shape[0]),np.int32(clImg.shape[1]),np.int32(clImg.shape[2]),
                         np.int32(FSIZE),np.float32(SIGMA))

        return self.dev.readBuffer(clBuf).reshape(clImg.shape[::-1])



    def test_nlmeans2d(self):
        proc = OCLProcessor(self.dev,absPath("kernels/nlmeans.cl"))

        data = np.ones((512,521)).astype(np.uint16)

        clImg = self.dev.createImage(data.shape[::-1])
        clBuf = self.dev.createBuffer(data.size)
        self.dev.writeImage(clImg,data.astype(np.uint16))

        BSIZE, FSIZE, SIGMA = 4, 2, 50

        proc.runKernel("run2d",clImg.shape,None,clImg,clBuf,
                         np.int32(clImg.shape[0]),np.int32(clImg.shape[1]),
                         np.int32(BSIZE), np.int32(FSIZE),np.float32(SIGMA))

        return self.dev.readBuffer(clBuf).reshape(clImg.shape).transpose()


    def test_nlmeans3d(self):
        proc = OCLProcessor(self.dev,absPath("kernels/nlmeans.cl"))

        data = np.ones((128,)*3).astype(np.uint16)


        clImg = self.dev.createImage(data.shape[::-1])
        clBuf = self.dev.createBuffer(data.size)
        self.dev.writeImage(clImg,data.astype(np.uint16))

        BSIZE, FSIZE, SIGMA = 2, 2, 150

        proc.runKernel("run3d",clImg.shape,None,clImg,clBuf,
                         np.int32(clImg.shape[0]),np.int32(clImg.shape[1]),np.int32(clImg.shape[2]),
                         np.int32(BSIZE), np.int32(FSIZE),np.float32(SIGMA))

        return self.dev.readBuffer(clBuf).reshape(clImg.shape[::-1])




if __name__ == '__main__':
    test  = TestBasic()
    test.setUp()

    out = test.test_mean3()
    # out = test.test_img3d()

    # import pylab

    # f = TestFilters()
    # f.setUp()

    # out  = f.test_nlmeans3d()
    # pylab.ion()
    # pylab.matshow(out[0,:,:])
