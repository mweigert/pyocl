#PyOCL


simple utility wrapper around pyopencl


### Installing


> pip install --user git+http://bitbucket.org/mweigert/pyocl


-----

### Usage


	:::python

	#create a device
    dev = OCLDevice(useGPU=True, useDevice = 0, printInfo = True)

    #create a OpenCL buffers/imgs
    img = dev.createImage((N,N), dtype = float32 )
    buf = dev.createBuffer(N*N, dtype = float32)

    # setup processor for device from opencl kernel file
    proc = OCLProcessor(dev,"kernel.cl")

    # write to device
    dev.writeImage(img,data)

    # compute
    proc.runKernel("kernelName",(N,N),None,img,buf,**params)

    # read from device
    out = dev.readBuffer(buf, dtype = float32).reshape(img.shape)
